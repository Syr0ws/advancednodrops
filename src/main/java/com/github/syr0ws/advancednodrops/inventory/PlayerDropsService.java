package com.github.syr0ws.advancednodrops.inventory;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.UUID;

public class PlayerDropsService {

    private final Map<UUID, PlayerDrops> drops = new HashMap<>();

    public void keepDrops(PlayerDrops playerDrops) {

        if(playerDrops == null) {
            throw new IllegalArgumentException("playerDrops cannot be null");
        }

        this.drops.put(playerDrops.getPlayerUUID(), playerDrops);
    }

    public void restoreDrops(Player player) {

        if(player == null) {
            throw new IllegalArgumentException("player cannot be null");
        }

        // Checking that the player is online.
        if(!player.isOnline()) {
            return;
        }

        UUID uuid = player.getUniqueId();

        // Checking that the player has drops.
        if(!this.drops.containsKey(uuid)) {
            return;
        }

        PlayerInventory inventory = player.getInventory();

        // Restoring drops.
        PlayerDrops playerDrops = this.drops.get(uuid);
        playerDrops.getItems().forEach(inventory::setItem);

        this.drops.remove(uuid);
    }

    public void restoreAll() {
        new HashSet<>(this.drops.keySet()).stream()
                .map(Bukkit::getPlayer)
                .forEach(this::restoreDrops);
    }

    public boolean hasDrops(Player player) {

        if(player == null) {
            throw new IllegalArgumentException("player cannot be null");
        }

        UUID uuid = player.getUniqueId();

        return this.drops.containsKey(uuid);
    }
}
