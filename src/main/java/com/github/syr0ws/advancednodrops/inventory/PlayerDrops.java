package com.github.syr0ws.advancednodrops.inventory;

import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PlayerDrops {

    private final UUID uuid;
    private final Map<Integer, ItemStack> items;

    public PlayerDrops(UUID uuid, Map<Integer, ItemStack> items) {

        if(uuid == null) {
            throw new IllegalArgumentException("uuid cannot be null");
        }

        if(items == null) {
            throw new IllegalArgumentException("items cannot be null");
        }

        this.uuid = uuid;
        this.items = items;
    }

    public UUID getPlayerUUID() {
        return uuid;
    }

    public Map<Integer, ItemStack> getItems() {
        return new HashMap<>(this.items);
    }
}
