package com.github.syr0ws.advancednodrops;

import com.github.syr0ws.advancednodrops.command.CommandAdvancedNoDrops;
import com.github.syr0ws.advancednodrops.drop.Drop;
import com.github.syr0ws.advancednodrops.drop.DropDao;
import com.github.syr0ws.advancednodrops.drop.DropService;
import com.github.syr0ws.advancednodrops.exception.DropLoadingException;
import com.github.syr0ws.advancednodrops.inventory.PlayerDropsService;
import com.github.syr0ws.advancednodrops.listener.PlayerListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AdvancedNoDropsPlugin extends JavaPlugin {

    private DropDao dropDao;
    private DropService dropService;
    private PlayerDropsService playerDropsService;

    @Override
    public void onEnable() {

        // Loading default config file.
        this.loadConfiguration();

        // Instantiating internal objects.
        this.dropDao = new DropDao(super.getLogger());
        this.dropService = new DropService();
        this.playerDropsService = new PlayerDropsService();

        // Loading drops.
        this.loadDrops();

        // Registering.
        this.registerListeners();
        this.registerCommands();
    }

    @Override
    public void onDisable() {

        // Restoring all the remaining drops when the plugin is stopped.
        this.playerDropsService.restoreAll();
    }

    private void loadConfiguration() {
        super.saveDefaultConfig();
    }

    public boolean reloadConfiguration() {
        super.reloadConfig();
        return this.loadDrops();
    }

    private void registerListeners() {
        PluginManager manager = Bukkit.getPluginManager();
        manager.registerEvents(new PlayerListener(this.dropService, this.playerDropsService), this);
    }

    private void registerCommands() {
        super.getCommand("advancednodrops").setExecutor(new CommandAdvancedNoDrops(this));
    }

    private boolean loadDrops() {

        Logger logger = super.getLogger();

        try {

            Set<Drop> drops = this.dropDao.loadDrops(super.getConfig());
            this.dropService.setDrops(drops);

            logger.log(Level.INFO, String.format("Drops have been loaded successfully (%d drops)", drops.size()));

            return true;

        } catch (DropLoadingException exception) {
            logger.log(Level.SEVERE, "An error occurred while loading drops", exception);
            return false;
        }
    }
}
