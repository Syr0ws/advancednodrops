package com.github.syr0ws.advancednodrops.util;

import org.bukkit.ChatColor;

public class TextUtil {

    public static String parseColors(String text) {
        return ChatColor.translateAlternateColorCodes('&', text);
    }
}
