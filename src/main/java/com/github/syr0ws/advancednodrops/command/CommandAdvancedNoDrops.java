package com.github.syr0ws.advancednodrops.command;

import com.github.syr0ws.advancednodrops.AdvancedNoDropsPlugin;
import com.github.syr0ws.advancednodrops.util.TextUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

public class CommandAdvancedNoDrops implements CommandExecutor {

    private final AdvancedNoDropsPlugin plugin;

    public CommandAdvancedNoDrops(AdvancedNoDropsPlugin plugin) {

        if(plugin == null) {
            throw new IllegalArgumentException("plugin cannot be null");
        }

        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        FileConfiguration config = this.plugin.getConfig();
        ConfigurationSection section = config.getConfigurationSection("command-advancednodrops");

        if(!sender.hasPermission("advancednodrops.command")) {
            String message = section.getString("no-permission", "");
            sender.sendMessage(TextUtil.parseColors(message));
            return true;
        }

        if(args.length != 1 || !args[0].equalsIgnoreCase("reload")) {
            String message = section.getString("usages", "");
            sender.sendMessage(TextUtil.parseColors(message));
            return true;
        }

        boolean reloaded = this.plugin.reloadConfiguration();

        String message = section.getString(reloaded ? "reload-success" : "reload-error", "");
        sender.sendMessage(TextUtil.parseColors(message));

        return true;
    }
}
