package com.github.syr0ws.advancednodrops.exception;

public class DropLoadingException extends Exception {

    public DropLoadingException(String message) {
        super(message);
    }

    public DropLoadingException(String message, Throwable cause) {
        super(message, cause);
    }

    public DropLoadingException(Throwable cause) {
        super(cause);
    }
}
