package com.github.syr0ws.advancednodrops.listener;

import com.github.syr0ws.advancednodrops.drop.Drop;
import com.github.syr0ws.advancednodrops.drop.DropService;
import com.github.syr0ws.advancednodrops.inventory.PlayerDrops;
import com.github.syr0ws.advancednodrops.inventory.PlayerDropsService;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.*;

public class PlayerListener implements Listener {

    private final DropService dropService;
    private final PlayerDropsService playerDropsService;

    public PlayerListener(DropService dropService, PlayerDropsService playerDropsService) {

        if(dropService == null) {
            throw new IllegalArgumentException("dropService cannot be null");
        }

        if(playerDropsService == null) {
            throw new IllegalArgumentException("playerDropsService cannot be null");
        }

        this.dropService = dropService;
        this.playerDropsService = playerDropsService;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerDeath(PlayerDeathEvent event) {

        Player player = event.getEntity();
        List<ItemStack> drops = event.getDrops();

        // Preventing the detected drops from dropping.
        drops.removeIf(item -> this.dropService.isDrop(player, item));

        // Listing drops that need to be kept.
        this.keepPlayerDrops(event);
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {

        Player player = event.getPlayer();

        if(this.playerDropsService.hasDrops(player)) {
            this.playerDropsService.restoreDrops(player);
        }
    }

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent event) {

        Player player = event.getPlayer();

        if(this.playerDropsService.hasDrops(player)) {
            this.playerDropsService.restoreDrops(player);
        }
    }

    private PlayerDrops getKeptDrops(Player player) {

        PlayerInventory inventory = player.getInventory();
        ItemStack[] contents = inventory.getContents();

        Map<Integer, ItemStack> items = new HashMap<>();

        for(int slot = 0; slot < inventory.getSize(); slot++) {

            ItemStack item = contents[slot];

            if(item == null) {
                continue;
            }

            // If there is at least one drop with the keep property, keeping the item.
            Optional<Drop> dropOptional = this.dropService.getDrop(player, item).stream()
                    .filter(Drop::keep)
                    .findFirst();

            if(dropOptional.isPresent()) {
                items.put(slot, item);
            }
        }

        return new PlayerDrops(player.getUniqueId(), items);
    }

    private void keepPlayerDrops(PlayerDeathEvent event) {

        Player player = event.getEntity();
        PlayerDrops drops = this.getKeptDrops(player);
        Map<Integer, ItemStack> items = drops.getItems();

        // Case in which no drops are to be kept after player death.
        if(items.isEmpty()) {
            return;
        }

        // Storing drops into the service.
        this.playerDropsService.keepDrops(drops);
    }
}
