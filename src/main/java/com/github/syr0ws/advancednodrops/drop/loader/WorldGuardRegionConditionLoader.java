package com.github.syr0ws.advancednodrops.drop.loader;

import com.github.syr0ws.advancednodrops.drop.condition.DropCondition;
import com.github.syr0ws.advancednodrops.drop.condition.WorldGuardRegionCondition;
import org.bukkit.configuration.ConfigurationSection;

import java.util.HashSet;
import java.util.List;

public class WorldGuardRegionConditionLoader implements DropConditionLoader {

    private static final String REGIONS_KEY = "worldguard-regions";

    @Override
    public DropCondition loadCondition(ConfigurationSection section) throws Exception {

        List<String> worlds = section.getStringList(REGIONS_KEY);

        return new WorldGuardRegionCondition(new HashSet<>(worlds));
    }

    @Override
    public boolean canLoad(ConfigurationSection section) {
        return section.isList(REGIONS_KEY);
    }
}
