package com.github.syr0ws.advancednodrops.drop;

import com.github.syr0ws.advancednodrops.drop.condition.DropCondition;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class DropService {

    private final Set<Drop> drops = new HashSet<>();

    public boolean isDrop(Player player, ItemStack item) {
        return this.drops.stream().anyMatch(drop -> {
            Set<DropCondition> checks = drop.getConditions();
            return checks.stream().allMatch(check -> check.isDrop(player, item));
        });
    }

    public Set<Drop> getDrop(Player player, ItemStack item) {
        return this.drops.stream().filter(drop -> {
            Set<DropCondition> conditions = drop.getConditions();
            return conditions.stream().allMatch(check -> check.isDrop(player, item));
        }).collect(Collectors.toSet());
    }

    public void setDrops(Set<Drop> drops) {

        if(drops == null) {
            throw new IllegalArgumentException("drops is null");
        }

        this.drops.clear();
        this.drops.addAll(drops);
    }

    public Set<Drop> getDrops() {
        return Collections.unmodifiableSet(this.drops);
    }
}
