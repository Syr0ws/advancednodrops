package com.github.syr0ws.advancednodrops.drop.condition;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.util.Location;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import com.sk89q.worldguard.protection.regions.RegionQuery;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.IllegalPluginAccessException;
import org.bukkit.plugin.PluginManager;

import java.util.Set;

public class WorldGuardRegionCondition implements DropCondition {

    private final Set<String> regions;

    public WorldGuardRegionCondition(Set<String> regions) {

        if(regions == null) {
            throw new IllegalArgumentException("regions cannot be null.");
        }

        this.regions = regions;
    }

    @Override
    public boolean isDrop(Player player, ItemStack item) {

        PluginManager pluginManager = Bukkit.getPluginManager();

        // Checking that WorldGuard is enabled.
        if(!pluginManager.isPluginEnabled("WorldGuard")) {
            throw new IllegalPluginAccessException("WorldGuard is not enabled");
        }

        // Finding the regions the player is in and checking if one of these
        // is a drop region.
        Location location = BukkitAdapter.adapt(player.getLocation());

        WorldGuard worldGuard = WorldGuard.getInstance();
        RegionContainer regionContainer = worldGuard.getPlatform().getRegionContainer();
        RegionQuery regionQuery = regionContainer.createQuery();
        ApplicableRegionSet regionSet = regionQuery.getApplicableRegions(location);

        return regionSet.getRegions().stream().anyMatch(region -> this.regions.contains(region.getId()));
    }
}
