package com.github.syr0ws.advancednodrops.drop.condition;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Set;

public class PermissionCondition implements DropCondition {

    private final Set<String> permissions;

    public PermissionCondition(Set<String> permissions) {

        if(permissions == null) {
            throw new IllegalArgumentException("permissions cannot be null.");
        }

        this.permissions = permissions;
    }

    @Override
    public boolean isDrop(Player player, ItemStack item) {
        return this.permissions.stream().anyMatch(player::hasPermission);
    }
}
