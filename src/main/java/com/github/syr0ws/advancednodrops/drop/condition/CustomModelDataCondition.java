package com.github.syr0ws.advancednodrops.drop.condition;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class CustomModelDataCondition implements DropCondition {

    private final int customModelData;

    public CustomModelDataCondition(int customModelData) {
        this.customModelData = customModelData;
    }

    @Override
    public boolean isDrop(Player player, ItemStack item) {

        if(!item.hasItemMeta()) {
            return false;
        }

        ItemMeta meta = item.getItemMeta();

        return meta.hasCustomModelData() && meta.getCustomModelData() == this.customModelData;
    }
}
