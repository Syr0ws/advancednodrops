package com.github.syr0ws.advancednodrops.drop.loader;

import com.github.syr0ws.advancednodrops.drop.condition.CustomModelDataCondition;
import com.github.syr0ws.advancednodrops.drop.condition.DropCondition;
import com.github.syr0ws.advancednodrops.exception.DropLoadingException;
import org.bukkit.configuration.ConfigurationSection;

public class CustomModelDataConditionLoader implements DropConditionLoader {

    private static final String CUSTOM_MODEL_DATA_KEY = "custom-model-data";

    @Override
    public DropCondition loadCondition(ConfigurationSection section) throws Exception {

        if(!section.isInt(CUSTOM_MODEL_DATA_KEY)) {
            throw new DropLoadingException(String.format("Property %s.%s is not a valid custom model data", section.getCurrentPath(), CUSTOM_MODEL_DATA_KEY));
        }

        int customModelData = section.getInt(CUSTOM_MODEL_DATA_KEY);

        return new CustomModelDataCondition(customModelData);
    }

    @Override
    public boolean canLoad(ConfigurationSection section) {
        return section.isSet("custom-model-data");
    }
}
