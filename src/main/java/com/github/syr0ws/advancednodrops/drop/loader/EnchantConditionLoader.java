package com.github.syr0ws.advancednodrops.drop.loader;

import com.github.syr0ws.advancednodrops.drop.condition.DropCondition;
import com.github.syr0ws.advancednodrops.drop.condition.EnchantCondition;
import com.github.syr0ws.advancednodrops.drop.operation.EnchantContainsOperation;
import com.github.syr0ws.advancednodrops.drop.operation.EnchantEqualsOperation;
import com.github.syr0ws.advancednodrops.drop.operation.EnchantOperation;
import com.github.syr0ws.advancednodrops.drop.operation.OperationType;
import com.github.syr0ws.advancednodrops.exception.DropLoadingException;
import org.bukkit.configuration.ConfigurationSection;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EnchantConditionLoader implements DropConditionLoader {

    private static final String ENCHANTS_KEY = "enchants";
    private static final String VALUE_KEY = "value";
    private static final String OPERATION_KEY = "operation";
    private static final Pattern PATTERN = Pattern.compile("^([a-zA-Z]+ ([1-9]|[1-9][0-9]*))|([a-zA-Z]+)$");

    @Override
    public DropCondition loadCondition(ConfigurationSection section) throws Exception {

        // Case in which the path is a list -> EQUALS operation.
        if(section.isList(ENCHANTS_KEY)) {
            Map<String, Integer> enchants = this.getEnchants(section, ENCHANTS_KEY);
            return new EnchantCondition(enchants, new EnchantEqualsOperation());
        }

        // Case in which the path is a section -> Specified operation.
        if(section.isConfigurationSection(ENCHANTS_KEY)) {

            ConfigurationSection enchantSection = section.getConfigurationSection(ENCHANTS_KEY);

            if(!enchantSection.isList(VALUE_KEY)) {
                throw new DropLoadingException(String.format("Property at %s.%s is not a list.", enchantSection.getCurrentPath(), VALUE_KEY));
            }

            Map<String, Integer> enchants = this.getEnchants(enchantSection, VALUE_KEY);
            EnchantOperation operation = this.getOperation(enchantSection);

            return new EnchantCondition(enchants, operation);
        }

        // Case in which the path is neither a list nor a section -> Invalid.
        throw new DropLoadingException(String.format("Property at %s.%s is not a valid enchants condition.", section.getCurrentPath(), ENCHANTS_KEY));
    }

    @Override
    public boolean canLoad(ConfigurationSection section) {
        return section.isSet(ENCHANTS_KEY);
    }

    private EnchantOperation getOperation(ConfigurationSection section) throws DropLoadingException {

        if(!section.isString(OPERATION_KEY)) {
            throw new DropLoadingException(String.format("Property at %s.%s is not a valid operation.", section.getCurrentPath(), OPERATION_KEY));
        }

        String operationTypeAsString = section.getString(OPERATION_KEY);

        OperationType operationType = OperationType.fromString(operationTypeAsString);

        return switch (operationType) {
            case EQUALS -> new EnchantEqualsOperation();
            case CONTAINS -> new EnchantContainsOperation();
            default -> throw new DropLoadingException(String.format("Operation %s at %s.%s is not supported.", operationType.name(), section.getCurrentPath(), OPERATION_KEY));
        };
    }

    private Map<String, Integer> getEnchants(ConfigurationSection section, String key) throws DropLoadingException {

        Map<String, Integer> enchants = new HashMap<>();

        for(String enchant : section.getStringList(key)) {

            Matcher matcher = PATTERN.matcher(enchant);

            if(!matcher.matches()) {
                throw new DropLoadingException(String.format("Malformed enchant '%s' at %s.%s", enchant, section.getCurrentPath(), key));
            }

            String[] array = enchant.split(" ");

            String enchantName = array[0];
            int level = array.length == 2 ? Integer.parseInt(array[1]) : -1;

            enchants.put(enchantName, level);
        }

        return enchants;
    }
}
