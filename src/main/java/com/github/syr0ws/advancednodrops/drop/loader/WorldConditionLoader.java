package com.github.syr0ws.advancednodrops.drop.loader;

import com.github.syr0ws.advancednodrops.drop.condition.DropCondition;
import com.github.syr0ws.advancednodrops.drop.condition.WorldCondition;
import org.bukkit.configuration.ConfigurationSection;

import java.util.HashSet;
import java.util.List;

public class WorldConditionLoader implements DropConditionLoader {

    private static final String WORLDS_KEY = "worlds";

    @Override
    public DropCondition loadCondition(ConfigurationSection section) throws Exception {

        List<String> worlds = section.getStringList(WORLDS_KEY);

        return new WorldCondition(new HashSet<>(worlds));
    }

    @Override
    public boolean canLoad(ConfigurationSection section) {
        return section.isList(WORLDS_KEY);
    }
}
