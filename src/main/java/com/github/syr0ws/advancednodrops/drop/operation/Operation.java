package com.github.syr0ws.advancednodrops.drop.operation;

public interface Operation<T> {

    boolean compare(T conditionValue, T testedValue);

    OperationType getType();
}
