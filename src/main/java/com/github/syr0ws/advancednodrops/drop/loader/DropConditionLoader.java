package com.github.syr0ws.advancednodrops.drop.loader;

import com.github.syr0ws.advancednodrops.drop.condition.DropCondition;
import org.bukkit.configuration.ConfigurationSection;

public interface DropConditionLoader {

    DropCondition loadCondition(ConfigurationSection section) throws Exception;

    boolean canLoad(ConfigurationSection section);
}
