package com.github.syr0ws.advancednodrops.drop.condition;

import com.github.syr0ws.advancednodrops.drop.operation.EnchantOperation;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Map;
import java.util.stream.Collectors;

public class EnchantCondition implements DropCondition {

    private final Map<String, Integer> enchantments;
    private final EnchantOperation operation;

    public EnchantCondition(Map<String, Integer> enchantments, EnchantOperation operation) {

        if(enchantments == null || enchantments.isEmpty()) {
            throw new IllegalArgumentException("enchantments cannot be null or empty");
        }

        if(operation == null) {
            throw new IllegalArgumentException("operation cannot be null");
        }

        this.enchantments = enchantments;
        this.operation = operation;
    }

    @Override
    public boolean isDrop(Player player, ItemStack item) {

        Map<String, Integer> enchantments = item.getEnchantments().entrySet().stream()
                .collect(Collectors.toMap(entry -> {
                    Enchantment enchantment = entry.getKey();
                    return enchantment.getKey().getKey();
                }, Map.Entry::getValue));

        return this.operation.compare(this.enchantments, enchantments);
    }
}
