package com.github.syr0ws.advancednodrops.drop.operation;

import java.util.Collection;

public class StringCollectionEquals extends StringCollectionOperation {

    @Override
    public boolean compare(Collection<String> conditionValue, Collection<String> testedValue) {
        return conditionValue.equals(testedValue);
    }

    @Override
    public OperationType getType() {
        return OperationType.EQUALS;
    }
}
