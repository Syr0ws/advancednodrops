package com.github.syr0ws.advancednodrops.drop.loader;

import com.github.syr0ws.advancednodrops.drop.condition.DropCondition;
import com.github.syr0ws.advancednodrops.drop.condition.MaterialCondition;
import com.github.syr0ws.advancednodrops.exception.DropLoadingException;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class MaterialConditionLoader implements DropConditionLoader {

    private static final String MATERIAL_KEY = "material";

    @Override
    public DropCondition loadCondition(ConfigurationSection section) throws Exception {

        // Case in which the path is a string.
        if(section.isString(MATERIAL_KEY)) {
            String materialName = section.getString(MATERIAL_KEY);
            Material material = this.loadMaterial(section, materialName);
            return new MaterialCondition(material);
        }

        // Case in which the path is a list.
        if(section.isList(MATERIAL_KEY)) {

            List<String> materialNames = section.getStringList(MATERIAL_KEY);

            Set<Material> materials = materialNames.stream().map(materialName -> {
                try {
                    return this.loadMaterial(section, materialName);
                } catch (Exception exception) {
                    throw new RuntimeException(exception);
                }
            }).collect(Collectors.toSet());

            return new MaterialCondition(materials);
        }

        throw new DropLoadingException(String.format("Property at %s.%s is not a string or a list", section.getCurrentPath(), MATERIAL_KEY));
    }

    @Override
    public boolean canLoad(ConfigurationSection section) {
        return section.isSet(MATERIAL_KEY);
    }

    private Material loadMaterial(ConfigurationSection section, String materialName) throws Exception {

        Material material = Material.matchMaterial(materialName);

        if(material == null) {
            throw new DropLoadingException(String.format("Material '%s' not found at %s.%s", materialName, section.getCurrentPath(), MATERIAL_KEY));
        }

        return material;
    }
}
