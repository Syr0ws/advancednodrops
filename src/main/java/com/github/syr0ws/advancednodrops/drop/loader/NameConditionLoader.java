package com.github.syr0ws.advancednodrops.drop.loader;

import com.github.syr0ws.advancednodrops.drop.condition.DropCondition;
import com.github.syr0ws.advancednodrops.drop.condition.NameCondition;
import com.github.syr0ws.advancednodrops.drop.operation.Operation;
import com.github.syr0ws.advancednodrops.drop.operation.OperationType;
import com.github.syr0ws.advancednodrops.drop.operation.StringContains;
import com.github.syr0ws.advancednodrops.drop.operation.StringEquals;
import com.github.syr0ws.advancednodrops.exception.DropLoadingException;
import org.bukkit.configuration.ConfigurationSection;

public class NameConditionLoader implements DropConditionLoader {

    private static final String NAME_KEY = "name";
    private static final String VALUE_KEY = "value";
    private static final String OPERATION_KEY = "operation";

    @Override
    public DropCondition loadCondition(ConfigurationSection section) throws Exception {

        // Case in which the path is a string -> EQUALS operation.
        if(section.isString(NAME_KEY)) {
            String name = section.getString(NAME_KEY);
            return new NameCondition(name, new StringEquals());
        }

        // Case in which the path is a section -> Specified operation.
        if(section.isConfigurationSection(NAME_KEY)) {

            ConfigurationSection nameSection = section.getConfigurationSection(NAME_KEY);

            if(!nameSection.isString(VALUE_KEY)) {
                throw new DropLoadingException(String.format("Property at %s.%s is not a string.", nameSection.getCurrentPath(), VALUE_KEY));
            }

            String name = nameSection.getString(VALUE_KEY);
            Operation<String> operation = this.getOperation(nameSection);

            return new NameCondition(name, operation);
        }

        // Case in which the path is neither a string nor a section -> Invalid.
        throw new DropLoadingException(String.format("Property at %s.%s is not a string.", section.getCurrentPath(), NAME_KEY));
    }

    @Override
    public boolean canLoad(ConfigurationSection section) {
        return section.isSet(NAME_KEY);
    }

    private Operation<String> getOperation(ConfigurationSection section) throws DropLoadingException {

        if(!section.isString(OPERATION_KEY)) {
            throw new DropLoadingException(String.format("Property at %s.%s is not a valid operation.", section.getCurrentPath(), OPERATION_KEY));
        }

        String operationTypeAsString = section.getString(OPERATION_KEY);

        OperationType operationType = OperationType.fromString(operationTypeAsString);

        return switch (operationType) {
            case EQUALS -> new StringEquals();
            case CONTAINS -> new StringContains();
            default -> throw new DropLoadingException(String.format("Operation %s at %s.%s is not supported.", operationType.name(), section.getCurrentPath(), OPERATION_KEY));
        };
    }
}
