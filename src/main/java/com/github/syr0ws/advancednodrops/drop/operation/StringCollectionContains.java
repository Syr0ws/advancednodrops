package com.github.syr0ws.advancednodrops.drop.operation;

import java.util.Collection;

public class StringCollectionContains extends StringCollectionOperation {

    @Override
    public boolean compare(Collection<String> conditionValue, Collection<String> testedValue) {
        return testedValue.stream().anyMatch(conditionValue::contains);
    }

    @Override
    public OperationType getType() {
        return OperationType.CONTAINS;
    }
}
