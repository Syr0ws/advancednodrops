package com.github.syr0ws.advancednodrops.drop;

import com.github.syr0ws.advancednodrops.drop.condition.DropCondition;

import java.util.Collections;
import java.util.Set;

public final class Drop {

    private final Set<DropCondition> conditions;
    private final boolean keep;

    public Drop(Set<DropCondition> conditions, boolean keep) {

        if(conditions == null) {
            throw new IllegalArgumentException("conditions cannot be null");
        }

        this.conditions = conditions;
        this.keep = keep;
    }

    public boolean keep() {
        return this.keep;
    }

    public Set<DropCondition> getConditions() {
        return Collections.unmodifiableSet(this.conditions);
    }
}
