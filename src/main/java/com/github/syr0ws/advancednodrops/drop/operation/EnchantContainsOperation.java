package com.github.syr0ws.advancednodrops.drop.operation;

import java.util.Map;

public class EnchantContainsOperation extends EnchantOperation {

    @Override
    public boolean compare(Map<String, Integer> conditionValue, Map<String, Integer> testedValue) {
        // Here, all the enchantments of the condition must be present in the item's enchantments.
        return conditionValue.entrySet().stream().allMatch(entry -> {

            String enchantment = entry.getKey();
            int conditionLevel = entry.getValue();

            // The item doesn't have the required enchantment.
            if(!testedValue.containsKey(enchantment)) {
                return false;
            }

            // No level condition.
            if(conditionLevel < 1) {
                return true;
            }

            Integer testedLevel = testedValue.get(enchantment);

            return conditionLevel == testedLevel;
        });
    }

    @Override
    public OperationType getType() {
        return null;
    }
}
