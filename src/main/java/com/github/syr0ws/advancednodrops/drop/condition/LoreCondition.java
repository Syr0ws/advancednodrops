package com.github.syr0ws.advancednodrops.drop.condition;

import com.github.syr0ws.advancednodrops.drop.operation.Operation;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Collection;
import java.util.List;

public class LoreCondition implements DropCondition {

    private final List<String> lore;
    private final Operation<Collection<String>> operation;

    public LoreCondition(List<String> lore, Operation<Collection<String>> operation) {

        if(lore == null) {
            throw new IllegalArgumentException("lore cannot be null");
        }

        if(operation == null) {
            throw new IllegalArgumentException("operation cannot be null");
        }

        this.lore = lore;
        this.operation = operation;
    }

    @Override
    public boolean isDrop(Player player, ItemStack item) {

        ItemMeta meta = item.getItemMeta();

        if(meta == null) {
            return false;
        }

        if(!meta.hasLore()) {
            return false;
        }

        List<String> lore = meta.getLore();

        return this.operation.compare(this.lore, lore);
    }
}
