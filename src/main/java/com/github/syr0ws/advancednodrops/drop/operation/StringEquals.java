package com.github.syr0ws.advancednodrops.drop.operation;

public class StringEquals extends StringOperation {

    @Override
    public boolean compare(String conditionValue, String testedValue) {
        return conditionValue.equals(testedValue);
    }

    @Override
    public OperationType getType() {
        return OperationType.EQUALS;
    }
}
