package com.github.syr0ws.advancednodrops.drop.condition;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashSet;
import java.util.Set;

public class MaterialCondition implements DropCondition {

    private final Set<Material> materials = new HashSet<>();

    public MaterialCondition(Set<Material> materials) {

        if(materials == null) {
            throw new IllegalArgumentException("materials cannot be null");
        }

        this.materials.addAll(materials);
    }

    public MaterialCondition(Material material) {

        if(material == null) {
            throw new IllegalArgumentException("material cannot be null");
        }

        this.materials.add(material);
    }

    @Override
    public boolean isDrop(Player player, ItemStack item) {

        Material material = item.getType();

        return this.materials.contains(material);
    }
}
