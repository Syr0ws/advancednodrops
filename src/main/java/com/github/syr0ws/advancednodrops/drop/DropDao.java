package com.github.syr0ws.advancednodrops.drop;

import com.github.syr0ws.advancednodrops.drop.condition.DropCondition;
import com.github.syr0ws.advancednodrops.drop.loader.*;
import com.github.syr0ws.advancednodrops.exception.DropLoadingException;
import org.bukkit.configuration.ConfigurationSection;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class DropDao {

    private final Logger logger;
    private final Set<DropConditionLoader> loaders = new HashSet<>();

    public DropDao(Logger logger) {

        if(logger == null) {
            throw new IllegalArgumentException("logger cannot be null");
        }

        this.logger = logger;
        this.registerConditionLoaders();
    }

    public Set<Drop> loadDrops(ConfigurationSection section) throws DropLoadingException {

        ConfigurationSection dropsSection = section.getConfigurationSection("drops");

        if(dropsSection == null) {
            throw new DropLoadingException("Section 'drops' not found");
        }

        Set<Drop> drops = new HashSet<>();

        for(String key : dropsSection.getKeys(false)) {

            // Checking that the key is a section.
            ConfigurationSection dropSection = dropsSection.getConfigurationSection(key);

            if(dropSection == null) {
                throw new DropLoadingException(String.format("Path '%s.%s' is not a section", dropsSection.getCurrentPath(), key));
            }

            // Loading each drop individually.
            try {
                Drop drop = this.loadDrop(dropSection);
                drops.add(drop);
            } catch (java.lang.Exception exception) {
                this.logger.log(Level.SEVERE, "An error occurred while loading drops", exception);
            }
        }

        return drops;
    }

    private void registerConditionLoaders() {
        this.loaders.add(new MaterialConditionLoader());
        this.loaders.add(new NameConditionLoader());
        this.loaders.add(new LoreConditionLoader());
        this.loaders.add(new CustomModelDataConditionLoader());
        this.loaders.add(new EnchantConditionLoader());
        this.loaders.add(new WorldConditionLoader());
        this.loaders.add(new PermissionConditionLoader());
        this.loaders.add(new WorldGuardRegionConditionLoader());
    }

    private Drop loadDrop(ConfigurationSection section) throws Exception {

        // Loading conditions.
        Set<DropCondition> conditions = this.loaders.stream()
                .filter(loader -> loader.canLoad(section))
                .map(loader -> {
                    try {
                        return loader.loadCondition(section);
                    } catch (Exception exception) {
                        throw new RuntimeException(exception);
                    }
                })
                .collect(Collectors.toSet());

        // Keep option.
        boolean keep = section.getBoolean("keep", false);

        // Build.
        try {
            return new Drop(conditions, keep);
        } catch (java.lang.Exception exception) {
            throw new DropLoadingException(exception);
        }
    }
}
