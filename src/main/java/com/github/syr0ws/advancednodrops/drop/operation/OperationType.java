package com.github.syr0ws.advancednodrops.drop.operation;

import java.util.Arrays;

public enum OperationType {

    EQUALS, CONTAINS;

    public static OperationType fromString(String typeAsString) {

        String upper = typeAsString.toUpperCase();

        return Arrays.stream(OperationType.values())
                .filter(type -> type.name().equals(upper))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Invalid operation type: " + typeAsString));
    }
}
