package com.github.syr0ws.advancednodrops.drop.operation;

import java.util.Map;

public class EnchantEqualsOperation extends EnchantOperation {

    @Override
    public boolean compare(Map<String, Integer> conditionValue, Map<String, Integer> testedValue) {
        // Here, all the enchantments of the item must be present in the condition's enchantments.
        return testedValue.entrySet().stream().allMatch(entry -> {

            String enchantment = entry.getKey();
            int level = entry.getValue();

            // The item doesn't have the required enchantment.
            if(!conditionValue.containsKey(enchantment)) {
                return false;
            }

            Integer conditionLevel = conditionValue.get(enchantment);

            // No level condition.
            if(conditionLevel < 1) {
                return true;
            }

            return level == conditionLevel;
        });
    }

    @Override
    public OperationType getType() {
        return OperationType.EQUALS;
    }
}
