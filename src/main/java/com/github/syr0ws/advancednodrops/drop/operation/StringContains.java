package com.github.syr0ws.advancednodrops.drop.operation;

public class StringContains extends StringOperation {

    @Override
    public boolean compare(String conditionValue, String testedValue) {
        return testedValue.contains(conditionValue);
    }

    @Override
    public OperationType getType() {
        return OperationType.CONTAINS;
    }
}
