package com.github.syr0ws.advancednodrops.drop.loader;

import com.github.syr0ws.advancednodrops.drop.condition.DropCondition;
import com.github.syr0ws.advancednodrops.drop.condition.LoreCondition;
import com.github.syr0ws.advancednodrops.drop.operation.Operation;
import com.github.syr0ws.advancednodrops.drop.operation.OperationType;
import com.github.syr0ws.advancednodrops.drop.operation.StringCollectionContains;
import com.github.syr0ws.advancednodrops.drop.operation.StringCollectionEquals;
import com.github.syr0ws.advancednodrops.exception.DropLoadingException;
import org.bukkit.configuration.ConfigurationSection;

import java.util.Collection;
import java.util.List;

public class LoreConditionLoader implements DropConditionLoader {

    private static final String LORE_KEY = "lore";
    private static final String VALUE_KEY = "value";
    private static final String OPERATION_KEY = "operation";

    @Override
    public DropCondition loadCondition(ConfigurationSection section) throws Exception {

        // Case in which the path is a list -> EQUALS operation.
        if(section.isList(LORE_KEY)) {
            List<String> lore = section.getStringList(LORE_KEY);
            return new LoreCondition(lore, new StringCollectionEquals());
        }

        // Case in which the path is a section -> Specified operation.
        if(section.isConfigurationSection(LORE_KEY)) {

            ConfigurationSection loreSection = section.getConfigurationSection(LORE_KEY);

            if(!loreSection.isList(VALUE_KEY)) {
                throw new DropLoadingException(String.format("Property at %s.%s is not a list.", loreSection.getCurrentPath(), VALUE_KEY));
            }

            List<String> lore = loreSection.getStringList(VALUE_KEY);
            Operation<Collection<String>> operation = this.getOperation(loreSection);

            return new LoreCondition(lore, operation);
        }

        // Case in which the path is neither a list nor a section -> Invalid.
        throw new DropLoadingException(String.format("Property at %s.%s is not a list.", section.getCurrentPath(), LORE_KEY));
    }

    @Override
    public boolean canLoad(ConfigurationSection section) {
        return section.isSet(LORE_KEY);
    }

    private Operation<Collection<String>> getOperation(ConfigurationSection section) throws DropLoadingException {

        if(!section.isString(OPERATION_KEY)) {
            throw new DropLoadingException(String.format("Property at %s.%s is not a valid operation.", section.getCurrentPath(), OPERATION_KEY));
        }

        String operationTypeAsString = section.getString(OPERATION_KEY);

        OperationType operationType = OperationType.fromString(operationTypeAsString);

        return switch (operationType) {
            case EQUALS -> new StringCollectionEquals();
            case CONTAINS -> new StringCollectionContains();
            default -> throw new DropLoadingException(String.format("Operation %s at %s.%s is not supported.", operationType.name(), section.getCurrentPath(), OPERATION_KEY));
        };
    }
}
