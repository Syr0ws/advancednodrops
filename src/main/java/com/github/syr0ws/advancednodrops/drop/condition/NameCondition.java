package com.github.syr0ws.advancednodrops.drop.condition;

import com.github.syr0ws.advancednodrops.drop.operation.Operation;
import com.github.syr0ws.advancednodrops.drop.operation.StringOperation;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class NameCondition implements DropCondition {

    private final String name;
    private final Operation<String> operation;

    public NameCondition(String name, Operation<String> operation) {

        if(name == null) {
            throw new IllegalArgumentException("name cannot be null");
        }

        if(operation == null) {
            throw new IllegalArgumentException("operation cannot be null");
        }

        this.name = name;
        this.operation = operation;
    }

    @Override
    public boolean isDrop(Player player, ItemStack item) {

        ItemMeta meta = item.getItemMeta();

        if(meta == null) {
            return false;
        }

        if(!meta.hasDisplayName()) {
            return false;
        }

        String displayName = meta.getDisplayName();

        return this.operation.compare(this.name, displayName);
    }
}
