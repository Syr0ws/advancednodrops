package com.github.syr0ws.advancednodrops.drop.condition;

import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Set;

public class WorldCondition implements DropCondition {

    private final Set<String> worlds;

    public WorldCondition(Set<String> worlds) {

        if(worlds == null) {
            throw new IllegalArgumentException("worlds cannot be null.");
        }

        this.worlds = worlds;
    }

    @Override
    public boolean isDrop(Player player, ItemStack item) {

        World world = player.getWorld();
        String worldName = world.getName();

        return this.worlds.contains(worldName);
    }
}
