package com.github.syr0ws.advancednodrops.drop.condition;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public interface DropCondition {

    boolean isDrop(Player player, ItemStack item);
}
