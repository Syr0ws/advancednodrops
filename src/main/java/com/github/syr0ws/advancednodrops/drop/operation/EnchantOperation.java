package com.github.syr0ws.advancednodrops.drop.operation;

import java.util.Map;

public abstract class EnchantOperation implements Operation<Map<String, Integer>> {

    protected boolean isDropEnchantment(String enchantment, int level, Map<String, Integer> reference) {

        // The item doesn't have the required enchantment.
        if(!reference.containsKey(enchantment)) {
            return false;
        }

        Integer conditionLevel = reference.get(enchantment);

        // No level condition.
        if(conditionLevel < 1) {
            return true;
        }

        return level == conditionLevel;
    }
}
