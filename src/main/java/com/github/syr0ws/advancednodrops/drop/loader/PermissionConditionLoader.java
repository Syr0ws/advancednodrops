package com.github.syr0ws.advancednodrops.drop.loader;

import com.github.syr0ws.advancednodrops.drop.condition.DropCondition;
import com.github.syr0ws.advancednodrops.drop.condition.PermissionCondition;
import org.bukkit.configuration.ConfigurationSection;

import java.util.HashSet;
import java.util.List;

public class PermissionConditionLoader implements DropConditionLoader {

    private static final String PERMISSIONS_KEY = "permissions";

    @Override
    public DropCondition loadCondition(ConfigurationSection section) throws Exception {

        List<String> permissions = section.getStringList(PERMISSIONS_KEY);

        return new PermissionCondition(new HashSet<>(permissions));
    }

    @Override
    public boolean canLoad(ConfigurationSection section) {
        return section.isList(PERMISSIONS_KEY);
    }
}
