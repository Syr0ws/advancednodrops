![Static Badge](https://img.shields.io/badge/Spigot_version-1.20%2B-blue)

# AdvancedNoDrops

AdvancedNoDrops is a fully configurable plugin to prevent items from dropping from the player's inventory when he dies. You can choose
between keeping the item in the player's inventory or removing it completely.

## Features

Configure a list of items you want to **prevent from being dropped** when a player **dies** based on conditions which can be **combined**.

Conditions can be based on **item's attributes** to target a **specific item**:
- Type
- Name
- Lore
- Custom model data
- Enchantments

Conditions can also be based on the **player** who **owns the items** when dying:
- Permissions
- Worlds
- WorldGuard regions

You can specify if the item should be **kept or not** in the player's inventory.

## Commands

- **/advancednodrops** reload to reload the configuration of the plugin.

## Permissions

- **advancednodrops.command** : Permission to use the /advancednodrops command.

## Installation

To install this plugin on your Spigot server, download it from [Spigot](https://www.spigotmc.org/resources/advancednodrops.116645/). Then, simply put it in the `plugins` folder of the server.

## Configuration

Here is an example of configuration:
```yaml
# You can use any combination of conditions you want.
# More information are available in the config.yml file of the plugin.
drops:
  # Keep diamond pickaxes in the player's inventory when he dies in the nether.
  diamond-pickaxe:
    material: "DIAMOND_PICKAXE"
    worlds:
      - "world_nether"
    keep: true
  
  # Keep diamond swords with the name 'Fire Sword!' in the player's inventory when he dies.
  diamond-sword:
    material: "DIAMOND_SWORD"
    name: "Fire Sword!"
    keep: true
    
  # Keep diamond swords which name contains 'Kill counter:' in the player's inventory when he dies.
  kill-counter:
    material: "DIAMOND_SWORD"
    name:
      value: "Kill counter:"
      operation: "CONTAINS"
  
  # Keep diamond helmets enchanted with protection 4 and unbreaking (no matter the level) 
  # in the player's inventory when he dies.
  enchanted-diamond-helmet:
    material: "DIAMOND_HELMET"
    enchants:
      - "protection 4"
      - "unbreaking"
  
  # Keep all the items in the player's inventory when he dies in the WorldGuard region named 'spawn'.
  spawn-drop:
    worldguard-regions:
      - "spawn"
    keep: true
  
  # Prevent cobblestone from dropping on the floor when a player dies.
  # Note: In this case, the items will not be kept in the player's inventory 
  # as the 'keep' property is not specified.
  cobblestone-drop:
    material: "COBBLESTONE"

  # Keep all the items in the player's inventory when he dies if it has  the permission 'items.keep.all'.
  keep-all-permission:
    permissions:
      - "items.keep.all"
    keep: true
  
  # Keep all the diamond armors in the player's inventory when he dies in the end or the nether.
  diamond-armors:
    material: 
      - "DIAMOND_HELMET"
      - "DIAMOND_CHESTPLATE"
      - "DIAMOND_LEGGINGS"
      - "DIAMOND_BOOTS"
    worlds:
      - "world_nether"
      - "world_the_end"
    keep: true
```

## Encountering a bug or wanting a new feature?

If you encounter a bug with the plugin or if you want a new feature, feel free to open an issue.